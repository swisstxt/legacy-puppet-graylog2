# Class: graylog2::web
#
# This module manages graylog2::web
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage: 
#   include graylog2::web
#
# [Remember: No empty lines between comments and class definition]
class graylog2::web (
    $mongodb_host = $graylog2::mongodb_host,
    $mongodb_username = $graylog2::mongodb_username,
    $mongodb_password = $graylog2::mongodb_password,
    $mongodb_replicaset = $graylog2::mongodb_replicaset,
  ) inherits graylog2 {

  include nginx

  class { "rbenv":
    user => 'root',
  }

  if !defined(Package["bundler"]) {
    package { "bundler":
      ensure => latest,
      provider => gem,
      require => Class["rbenv"],
    }
  }

  if !defined(Package["thin"]) {
    package { "thin":
      ensure => latest,
      provider => gem,
      require => Class["rbenv"],
    }
  }

  file { "${glBasePath}/src/graylog2-web-interface-${glVersion}.tar.gz":
    owner   => root,
    group   => root,
    mode    => 644,
    source  => "puppet:///modules/graylog2/graylog2-web-interface-${glVersion}.tar.gz",
  }

  exec { "graylog2-web-extract":
    path    => "/bin:/usr/bin:/usr/local/bin",
    cwd     => "${glBasePath}/src",
    command => "tar -xzf graylog2-web-interface-${glVersion}.tar.gz",
    require => File["${glBasePath}/src/graylog2-web-interface-${glVersion}.tar.gz"],
    creates => "${glBasePath}/src/graylog2-web-interface-${glVersion}",
  }

  file { "${glBasePath}/src/graylog2-web-interface-${glVersion}":
    ensure  => directory,
    owner   => nginx,
    group   => nginx,
    mode    => 755,
    require => Exec["graylog2-web-extract"],
  }

  file { "${glBasePath}/src/graylog2-web-interface-${glVersion}/log":
    ensure  => directory,
    owner   => nginx,
    group   => nginx,
    mode    => 755,
    require => File["${glBasePath}/src/graylog2-web-interface-${glVersion}"],
  }

  file { "${glBasePath}/src/graylog2-web-interface-${glVersion}/log/production.log":
    owner   => nginx,
    group   => nginx,
    mode    => 666,
    replace => false,
    content => "# graylog2 web interface log",
    require => File["${glBasePath}/src/graylog2-web-interface-${glVersion}/log"],
  }

  file { "${glBasePath}/web":
    ensure  => link,
    target  => "${glBasePath}/src/graylog2-web-interface-${glVersion}",
    require => Exec["graylog2-web-extract"],
  }

  file { "${glBasePath}/web/config/mongoid.yml":
    content => template("graylog2/graylog2-web-mongoid.yml.erb"),
    owner   => root,
    group   => root,
    mode    => 0644,
    require => File["${glBasePath}/web"],
  }
  
  exec { "Gemfile add thin":
    path 		=> "/bin",
    command => "echo 'gem \"thin\"' >> ${glBasePath}/src/graylog2-web-interface-${glVersion}/Gemfile",
    unless 	=> "grep -qFx 'gem \"thin\"' '${glBasePath}/src/graylog2-web-interface-${glVersion}/Gemfile'",
    require => File["${glBasePath}/web"],
    notify  => Exec["bundle graylog2-web"],
  }

  exec {"bundle graylog2-web":
    command   => "bundle --binstubs=${glBasePath}/src/graylog2-web-interface-${glVersion}${glBasePath}/src/graylog2-web-interface-${glVersion}/bin --path=${glBasePath}/src/graylog2-web-interface-${glVersion}/.bundle",
    cwd       => "${glBasePath}/src/graylog2-web-interface-${glVersion}",
    user      => 'root',
    group     => 'root',
    path      => "${glBasePath}/src/graylog2-web-interface-${glVersion}/bin:/opt/rbenv/shims:/opt/rbenv/bin:/bin:/usr/bin",
    require   => [Exec["Gemfile add thin"], Class['rbenv']],
    refreshonly => true,
    notify		=> Service["thin"],
  }

  file { "/etc/nginx/conf.d/graylog2-web.conf":
    content => template("graylog2/nginx.graylog2-web.conf.erb"),
    owner   => root,
    group   => root,
    mode    => 0644,
  }

  file { "/etc/thin":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 0755,
  }

  file { "/etc/thin/graylog2-web.yml":
    content => template("graylog2/thin.graylog2-web.yml.erb"),
    owner   => root,
    group   => root,
    mode    => 0644,
		require => File["/etc/thin"],
  }

  file { "/etc/init.d/thin":
    content => template("graylog2/thin.erb"),
    owner   => root,
    group   => root,
    mode    => 0755,
  }

  service { "thin":
    ensure => running,
    enable => true,
		hasrestart => true,
    hasstatus  => true,
    require => [File["/etc/init.d/thin", "/etc/thin/graylog2-web.yml"], Package["thin"], Exec["bundle graylog2-web"]],
  }
}
