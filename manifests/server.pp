# Class: graylog2::server
#
# This module manages graylog2::server
#
# Parameters:
#
# Actions:
#
# Requires: 
#
# Sample Usage:  
#   include graylog2::server
#
# [Remember: No empty lines between comments and class definition]
class graylog2::server (
    $syslog_protocol = 'udp',
    $mongodb_host = $graylog2::mongodb_host,
    $mongodb_username = $graylog2::mongodb_username,
    $mongodb_password = $graylog2::mongodb_password,
    $mongodb_replicaset = $graylog2::mongodb_replicaset,
  ) inherits graylog2 {

  class { elasticsearch: version => "0.19.11", install_root => "/opt" }

  file { "${glBasePath}/src/graylog2-server-${glVersion}.tar.gz":
    owner   => root,
    group   => root,
    mode    => 0644,
    source  => "puppet:///modules/graylog2/graylog2-server-${glVersion}.tar.gz",
  }

  exec { "graylog2-server-extract":
    path    => "/bin:/usr/bin:/usr/local/bin",
    cwd     => "${glBasePath}/src",
    command => "tar -xzf graylog2-server-${glVersion}.tar.gz",
    require => File["${glBasePath}/src/graylog2-server-${glVersion}.tar.gz"],
    creates => "${glBasePath}/src/graylog2-server-${glVersion}",
  }

  file { "${glBasePath}/server":
    ensure  => link,
    target  => "${glBasePath}/src/graylog2-server-${glVersion}",
    require => Exec["graylog2-server-extract"],
  }

  file { "/etc/graylog2.conf":
    content => template("graylog2/graylog2.conf.erb"),
    owner   => root,
    group   => root,
    mode    => 0644,
    notify  => Service["graylog2-server"],
  }
  
  file { "/etc/init.d/graylog2-server":
    content => template("graylog2/initd.graylog2-server.erb"),
    owner   => root,
    group   => root,
    mode    => 0755,
  }
  
  file { "/etc/cron.d/graylog2-server":
    content => template("graylog2/graylog2-server.cron.erb"),
    owner   => root,
    group   => root,
    mode    => 0644,
  }

  service { "graylog2-server":
    ensure => running,
    enable => true,
    hasrestart => true,
    require => [File["/etc/init.d/graylog2-server"], Class["rbenv"]],
  }
}
