# Class: graylog2
#
# This module manages graylog2
#
# Parameters: 
#   glVersion - the version of graylog2 being installed (eg. 0.9.6)
#   glBasePath - the location to which graylog2 is being installed (eg. /var/graylog2)
#
# Actions:
#
# Requires: 
#   java
#
# Sample Usage: // note this is useless without including graylog2::server and/or graylog2::web
#
# class { graylog2:
#   glVersion => "0.9.6p1",
#   install_mongodb => false,
#   mongodb_username => "graylog2",
#   mongodb_password => 'password',
#   mongodb_replicaset => 'mongo01:27017,mongo02:27017',
# }
#
# [Remember: No empty lines between comments and class definition]
class graylog2 ( 
		$glVersion = "0.9.6p1",
		$glBasePath = "/opt/graylog2",
		$install_mongodb = true,
  	$mongodb_host = "localhost",
    $mongodb_username = "graylog2",
    $mongodb_password = '',
    $mongodb_replicaset = '',
	) {

  if $install_mongodb {
    include mongodb
  }

  package { 'java-1.6.0-openjdk' :
			ensure => installed,
  }

  file { "${glBasePath}":
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755,
  }

  file { "${glBasePath}/src":
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755,
  }
}

